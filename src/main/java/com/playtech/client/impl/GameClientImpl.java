package com.playtech.client.impl;

import com.playtech.client.api.GameClient;
import com.playtech.game.protocol.FinishRoundRequest;
import com.playtech.game.protocol.StartRoundRequest;

public class GameClientImpl implements GameClient {
    @Override
    public void startRound(StartRoundRequest startRoundRequest) {

    }

    @Override
    public void finishRound(FinishRoundRequest finishRoundRequest) {

    }
}
